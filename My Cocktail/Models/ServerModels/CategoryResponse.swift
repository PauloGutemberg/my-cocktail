//
//  CategoryResponse.swift
//  My Cocktail
//
//  Created by Paulo Gutemberg on 11/09/19.
//  Copyright © 2019 Paulo Gutemberg. All rights reserved.
//

import Foundation

struct CategoryResponse: Codable {
	
	let strCategory: String
	
	enum CodingKeys : String, CodingKey {
		case strCategory = "strCategory"
		
	}
	
	func decode(jsonData: Data) -> CategoryResponse {
		let decoder = JSONDecoder()
		let category = try! decoder.decode(CategoryResponse.self, from: jsonData)
		print(category)
		return category
	}
}
