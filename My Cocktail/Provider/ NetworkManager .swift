//
//  MyCocktailProvider.swift
//  My Cocktail
//
//  Created by Paulo Gutemberg on 14/09/19.
//  Copyright © 2019 Paulo Gutemberg. All rights reserved.
//

import UIKit
import Moya

enum ErrorLocalized: Error {
	case localizedError(localizedDescription: String)
	
}

class NetworkManager: NSObject {
	
	static var provider = MoyaProvider<MyCocktail>()
	
		
	static func get(endpoint: MyCocktail, handler: @escaping (Response?, Error?) -> ()){
		provider.request(endpoint) { (result) in
			switch result {
			case .success(let response):
				/*print(response.statusCode)
				print(response.request?.description ?? "url error")
				print(String(bytes: response.data, encoding: .utf8)!)*/
				handler(response , nil)
				
			case .failure(let error):
				handler(nil, ErrorLocalized.localizedError(localizedDescription: error.localizedDescription))
			}
		}
	}
	
}
