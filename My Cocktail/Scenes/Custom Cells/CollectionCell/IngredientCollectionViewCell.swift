//
//  IngredientCollectionViewCell.swift
//  My Cocktail
//
//  Created by Paulo Gutemberg on 12/09/19.
//  Copyright © 2019 Paulo Gutemberg. All rights reserved.
//

import UIKit

class IngredientCollectionViewCell: UICollectionViewCell {
	
	@IBOutlet weak var imgIngredient: UIImageView!
	@IBOutlet weak var lbIngredient: UILabel!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
	}
}
