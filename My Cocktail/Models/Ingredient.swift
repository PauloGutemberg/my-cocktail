//
//  Ingredient.swift
//  My Cocktail
//
//  Created by Paulo Gutemberg on 11/09/19.
//  Copyright © 2019 Paulo Gutemberg. All rights reserved.
//

import Foundation

public struct Ingredient : Codable{
	var name: String?
	var measure: String?
	
	enum codingKeys: String, CodingKey {
		case name = "name"
		case measure = "measure"
	}
}
