//
//  Constants.swift
//  My Cocktail
//
//  Created by Paulo Gutemberg on 14/09/19.
//  Copyright © 2019 Paulo Gutemberg. All rights reserved.
//

import UIKit

let BASE_INGREDIENT_IMAGE_URL = "https://www.thecocktaildb.com/images/ingredients/"
let EXTENSION_IMAGE = ".png"
