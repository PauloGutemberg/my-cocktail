//
//  SearchResponse.swift
//  My Cocktail
//
//  Created by Paulo Gutemberg on 11/09/19.
//  Copyright © 2019 Paulo Gutemberg. All rights reserved.
//

import Foundation

struct SearchResponse: Codable {
	let drinks: Array<DrinkResponse>?
}
