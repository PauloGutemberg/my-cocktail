//
//  DrinkViewController.swift
//  My Cocktail
//
//  Created by Paulo Gutemberg on 12/09/19.
//  Copyright © 2019 Paulo Gutemberg. All rights reserved.
//

import UIKit

class DrinkViewController: UIViewController {
	
	var myCategory: Category?
	var drinks: [Drink] = []
	var filteredDrink:[Drink] = []
	
	@IBOutlet weak var imgErro: UIImageView!
	@IBOutlet weak var lbErro: UILabel!
	
	@IBOutlet weak var searchBar: UISearchBar!
	
	@IBOutlet weak var tableViewDrinks: UITableView!
	
	@IBOutlet weak var viewErro: UIView!
	
    override func viewDidLoad() {
        super.viewDidLoad()

		self.tableViewDrinks.dataSource = self
		self.tableViewDrinks.delegate = self
		self.searchBar.delegate = self
		
        self.setConfigNavigation()
		
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.loadData()
	}
	
	
	private func makeRequest(categoryMo: Category){
		Requester.getDrinkByCategory(category: categoryMo.strCategory) { (success, error) in
			if success {
				Singleton.sharedInstance.drinks = Singleton.sharedInstance.drinks.map{
					var mutableDrink = $0
					if let c = self.myCategory?.strCategory{
						if mutableDrink.category == nil{
							mutableDrink.category = c
						}
					}
					return mutableDrink
				}
				
				for drink in Singleton.sharedInstance.drinks {
					if drink.category == categoryMo.strCategory {
						self.drinks.append(drink)
						self.filteredDrink.append(drink)
					}
				}

				self.tableViewDrinks.reloadData()
			}
			self.hiddenTable()
		}
	}
	
	private func loadData(){
		if let categoryMo = self.myCategory {
			if(Singleton.sharedInstance.drinks.isEmpty){
				self.makeRequest(categoryMo: categoryMo)
			}else{
				var findValuesInSearch = false
				if let categoryName = self.myCategory?.strCategory {
					for drinkAux in Singleton.sharedInstance.drinks {
						if categoryName == drinkAux.category {
							findValuesInSearch = true
							self.drinks.append(drinkAux)
							self.filteredDrink.append(drinkAux)
						}
						
					}
				}
				
				if findValuesInSearch == false {
					self.makeRequest(categoryMo: categoryMo)
				}
			}
		}
	}
	
	private func setConfigNavigation(){
		
		let buttonBack = UIBarButtonItem(title: "Categories", style: .plain, target: self, action: #selector(segueToCategories))
		self.navigationItem.leftBarButtonItem  = buttonBack
	}
	
	@objc func segueToCategories(){
		performSegue(withIdentifier: "goToCategories", sender: nil)
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let drink = sender as? Drink, (segue.identifier == "goToDrinkDetailStoryboard") {
			if let destinationController = segue.destination as? UINavigationController {
				if let targetController = destinationController.topViewController as? DrinkDetailViewController {
					
					targetController.myDrink = drink
					if let categoryAux = self.myCategory {
						targetController.myCategory = categoryAux
					}
				}
			}
		}
	}
}


extension DrinkViewController:UITableViewDelegate{
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		performSegue(withIdentifier: "goToDrinkDetailStoryboard", sender: self.filteredDrink[indexPath.row])
	}
}

extension DrinkViewController:UITableViewDataSource{
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.filteredDrink.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "cellDrink", for: indexPath) as! DrinkTableViewCell
		
		cell.labelDrinkName.text = self.filteredDrink[indexPath.row].name
		if let urlStr = self.filteredDrink[indexPath.row].drinkThumbUrl {
			NetworkHelper.downloadImage(urlStr: urlStr, imageView: cell.imageDrink)
		}
		
		return cell
	}
	
	
}

extension DrinkViewController: UISearchBarDelegate{
	
	func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
		self.searchBar.endEditing(true)
	}
	
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		if searchText.isEmpty {
			self.filteredDrink = self.drinks
		} else {
			self.filteredDrink = self.drinks.filter({ (drink) -> Bool in
				drink.name?.lowercased().contains(searchText.lowercased()) ?? false
			})
		}
		
		
		if(filteredDrink.isEmpty){
			
			self.lbErro.text = "Your search for "+searchText+" did not match any results."
			self.imgErro.image = UIImage(named: "search_icon")
			self.viewErro.isHidden = false
			
		}else{
			self.viewErro.isHidden = true
			self.tableViewDrinks.isHidden = false
		}
		
		self.tableViewDrinks.reloadData()
	}
	
	private func hiddenTable(){
		
		if( self.drinks.isEmpty){
			
			self.viewErro.isHidden = false
		}
		else{
			
			self.tableViewDrinks.isHidden = false
			self.viewErro.isHidden = true
		}
		
	}
	
}
