//
//  CategoryResponse.swift
//  My Cocktail
//
//  Created by Paulo Gutemberg on 11/09/19.
//  Copyright © 2019 Paulo Gutemberg. All rights reserved.
//

import UIKit

struct CategoryResponse: Codable {
	
	var strCategory: String?
	
	enum CodingKeys: String, CodingKey {
		case strCategory
	}
	
}
