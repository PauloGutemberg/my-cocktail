//
//  Singleton.swift
//  My Cocktail
//
//  Created by Paulo Gutemberg on 14/09/19.
//  Copyright © 2019 Paulo Gutemberg. All rights reserved.
//

import Foundation

class Singleton: NSObject {
	
	static let sharedInstance: Singleton = Singleton()
	
	var categories : [Category] = []
	var drinks : [Drink] = []
	var singleDrink: [Drink] = []
	
}
