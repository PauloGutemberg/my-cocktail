//
//  DrinkDetailViewController.swift
//  My Cocktail
//
//  Created by Paulo Gutemberg on 12/09/19.
//  Copyright © 2019 Paulo Gutemberg. All rights reserved.
//

import UIKit

class DrinkDetailViewController: UIViewController {
	
	fileprivate let itemHeight: CGFloat = 140
	fileprivate let itemsPerRow: CGFloat = 2
	fileprivate let sectionInsets = UIEdgeInsets(top: 20.0, left: 20.0, bottom: 50.0, right: 20.0)
	
	var myDrink: Drink?
	var myCategory: Category?
	
	@IBOutlet weak var imgDrink: UIImageView!
	
	@IBOutlet weak var instructionsPrepare: UITextView!
	
	@IBOutlet weak var collectionViewIngredients: UICollectionView!
	var ingredients:[Ingredient] = []
	
	override func viewDidLoad() {
        super.viewDidLoad()

		self.collectionViewIngredients.dataSource = self
		self.collectionViewIngredients.delegate = self
		
		self.setConfigNavigation()
		
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.loadData()
	}
	
	private func setConfigNavigation(){
		if let drink = myDrink {
			navigationItem.title = drink.name
		}
			
		let buttonBack = UIBarButtonItem(title: "Drinks", style: .plain, target: self, action: #selector(segueToDrinks))
		self.navigationItem.leftBarButtonItem  = buttonBack
	}
	
	@objc func segueToDrinks(){
		performSegue(withIdentifier: "goToDrinks", sender: nil)
	}
	
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if (segue.identifier == "goToDrinks") {
			if let destinationController = segue.destination as? UINavigationController {
				if let targetController = destinationController.topViewController as? DrinkViewController {
					if let category = self.myCategory {
						targetController.myCategory = category						
					}
					
				}
			}
		}
	}

	private func getDataFromCache(id: String) -> Drink?{
		let drinks = Singleton.sharedInstance.drinks
		for drink in drinks {
			if drink.id == id {
				if let ingredients = drink.ingredients {
					if !(ingredients.isEmpty) {
						return drink
					}
				}
			}
		}
		return nil
	}
	
	
	private func saveDataInCache(id: String, ingredientes: [Ingredient], instructions: String){
		
		Singleton.sharedInstance.drinks = Singleton.sharedInstance.drinks.map{
			var mutableDrink = $0
			if $0.id == id {
				mutableDrink.instructions = instructions
				mutableDrink.ingredients = ingredientes
			}
			return mutableDrink
		}
		
		for d in Singleton.sharedInstance.drinks{
			print(d)
		}
		
	}
	
	private func loadDrink(drink: Drink?){
		if let instructions = drink?.instructions{
			self.instructionsPrepare.text = instructions
		}
		
		if let urlImg = drink?.drinkThumbUrl {
			NetworkHelper.downloadImage(urlStr: urlImg, imageView: self.imgDrink)
		}
		
		if let ingredientsAux = drink?.ingredients{
			for i in ingredientsAux{
				if let nameIngredient = i.name {
					if !(nameIngredient.isEmpty){
						self.ingredients.append(i)
					}
				}
			}
		}
	}
	
	private func loadData(){
		if let drinkMo = self.myDrink {
			if let idDrink = drinkMo.id {
				let drinkReturn = getDataFromCache(id: idDrink)
				if(drinkReturn != nil){
					self.myDrink = drinkReturn
					self.loadDrink(drink: self.myDrink)
					print("veio a data do cache")
				}else{
					print("nao veio data do cache")
					Requester.getDrinkByID(drinkIdentifier: idDrink) { (success, error) in
						if success {
							self.myDrink = Singleton.sharedInstance.singleDrink[0]
							self.loadDrink(drink: self.myDrink)
							self.saveDataInCache(id: idDrink, ingredientes:self.ingredients, instructions: self.instructionsPrepare.text)
							
							self.collectionViewIngredients.reloadData()
						}
					}
				}
				
			}
		}
	}

}


extension DrinkDetailViewController: UICollectionViewDataSource {
	
	func numberOfSections(in collectionView: UICollectionView) -> Int{
		return 1;
	}
	
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
		return self.ingredients.count
	}
	
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
		
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIngredient", for: indexPath) as! IngredientCollectionViewCell
		
		// Config Cell
		
		cell.layer.borderColor = UIColor.black.cgColor
		cell.layer.borderWidth = 1
		cell.clipsToBounds = true
		
		cell.layer.masksToBounds = true;
		cell.layer.cornerRadius = 6;
		
		//Loading itens
		
		if let ingredientName = ingredients[indexPath.row].name {
			cell.lbIngredient.text = ingredients[indexPath.row].name
			var ingredientImageUrl = BASE_INGREDIENT_IMAGE_URL + ingredientName + EXTENSION_IMAGE
			ingredientImageUrl = ingredientImageUrl.replacingOccurrences(of: " ", with: "%20")
			NetworkHelper.downloadImage(urlStr: ingredientImageUrl, imageView: cell.imgIngredient)
			
			//it's possible add the measure in cell of the collection view
		}
		return cell
	}
	
	func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
		
	}
	
	func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
		
	}
	
}

extension DrinkDetailViewController: UICollectionViewDelegate{
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		
	}
}

extension DrinkDetailViewController: UICollectionViewDelegateFlowLayout {
	
	func collectionView(_ collectionView: UICollectionView,
						layout collectionViewLayout: UICollectionViewLayout,
						sizeForItemAt indexPath: IndexPath) -> CGSize {
		let screenWidth = UIScreen.main.bounds.width
		
		let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
		let availableWidth = screenWidth - paddingSpace
		let widthPerItem = availableWidth / itemsPerRow
		
		return CGSize(width: widthPerItem, height: itemHeight)
	}
	
	
	func collectionView(_ collectionView: UICollectionView,
						layout collectionViewLayout: UICollectionViewLayout,
						insetForSectionAt section: Int) -> UIEdgeInsets {
		return sectionInsets
	}
	
	
	func collectionView(_ collectionView: UICollectionView,
						layout collectionViewLayout: UICollectionViewLayout,
						minimumLineSpacingForSectionAt section: Int) -> CGFloat {
		return sectionInsets.left
	}
	
}

