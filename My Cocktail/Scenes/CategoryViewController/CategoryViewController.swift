//
//  ViewController.swift
//  My Cocktail
//
//  Created by Paulo Gutemberg on 09/09/19.
//  Copyright © 2019 Paulo Gutemberg. All rights reserved.
//

import UIKit

class CategoryViewController: UIViewController {

	@IBOutlet weak var tableViewCategories: UITableView!
	
	var categories: [Category] = []
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.tableViewCategories.dataSource = self
		self.tableViewCategories.delegate = self
	}
	

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.loadData()
	
	}
	
	
	private func loadData(){
		if(Singleton.sharedInstance.categories.isEmpty){
			Requester.getCategories(completionHandler: {(success, error) in
				if success {
					self.categories = Singleton.sharedInstance.categories
					self.tableViewCategories.reloadData()
				}
			})
		}else{			
			self.categories = Singleton.sharedInstance.categories
		}
		self.categories.sort(by: {$0.strCategory > $1.strCategory})			
		
	}

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let category = sender as? Category, (segue.identifier == "goToDrinkStoryboard") {
			if let destinationController = segue.destination as? UINavigationController {
				if let targetController = destinationController.topViewController as? DrinkViewController {
					
					targetController.myCategory = category
				}
			}
		}
	}

}

extension CategoryViewController:UITableViewDelegate{
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		performSegue(withIdentifier: "goToDrinkStoryboard", sender: self.categories[indexPath.row])
	}
}

extension CategoryViewController:UITableViewDataSource{
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.categories.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "cellCategory", for: indexPath)
		
		cell.textLabel?.text = self.categories[indexPath.row].strCategory
		
		return cell
	}
	
	
}
