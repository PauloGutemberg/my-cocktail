//
//  DrinkTableViewCell.swift
//  My Cocktail
//
//  Created by Paulo Gutemberg on 12/09/19.
//  Copyright © 2019 Paulo Gutemberg. All rights reserved.
//

import UIKit

class DrinkTableViewCell: UITableViewCell {

	@IBOutlet weak var imageDrink: UIImageView!
	@IBOutlet weak var labelDrinkName: UILabel!
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
