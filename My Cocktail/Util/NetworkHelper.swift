//
//  NetworkHelper.swift
//  My Cocktail
//
//  Created by Paulo Gutemberg on 12/09/19.
//  Copyright © 2019 Paulo Gutemberg. All rights reserved.
//

import UIKit


class NetworkHelper {
	static func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
		URLSession.shared.dataTask(with: url) { data, response, error in
			completion(data, response, error)
			}.resume()
	}
	
	static func downloadImage(urlStr: String, imageView: UIImageView) {
		if let url = URL(string: urlStr) {
			getDataFromUrl(url: url) { data, response, error in
				guard let data = data, error == nil else { return }
				DispatchQueue.main.async() {
					imageView.image = UIImage(data: data)
				}
			}
		}
	}
}
