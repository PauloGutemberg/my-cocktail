//
//  Requester.swift
//  My Cocktail
//
//  Created by Paulo Gutemberg on 14/09/19.
//  Copyright © 2019 Paulo Gutemberg. All rights reserved.
//

import Foundation

import UIKit


class Requester: NSObject {
	
	static var arrayCategories:[Category] = Singleton.sharedInstance.categories
	static var arrayDrinks:[Drink] = Singleton.sharedInstance.drinks
	static var arraySingleDrinks:[Drink] = Singleton.sharedInstance.singleDrink
	
	static func getCategories(completionHandler completion:@escaping (_ success:Bool, _ error : NSError?) -> Void){
		
		NetworkManager.get(endpoint: .categories) { (response, errorResponse) in
			
			if let responseAux = response {				
				let categoriesServerModel = try! responseAux.map([CategoryResponse].self, atKeyPath: "drinks")
				for categoryServerModel in categoriesServerModel {
					if let nameCategory = categoryServerModel.strCategory {
						let categoryAux = Category(strCategory: nameCategory)
						self.arrayCategories.append(categoryAux)
					}
				}
				
				Singleton.sharedInstance.categories = self.arrayCategories
				completion(true, nil)
			
			} else if let error = errorResponse {
				print(error.localizedDescription)
				completion(false, nil)
			}
		}
	}
	
	static func getDrinkByCategory(category:String , completionHandler completion:@escaping (_ success:Bool, _ error : NSError?) -> Void){
		
		NetworkManager.get(endpoint: .drinksByCategory(categoryName: category)) { (response, errorResponse) in
			
			if let responseAux = response {
				
				let drinksServerModel = try! responseAux.map([DrinkResponse].self, atKeyPath: "drinks")
				self.arrayDrinks = ServerMapper.mapSeverToDrinkUIModels(drinks: drinksServerModel)
				
				for i in self.arrayDrinks {
					Singleton.sharedInstance.drinks.append(i)
				}
				
				completion(true, nil)
			
			} else if let error = errorResponse {
				print(error.localizedDescription)
				completion(false, nil)
			}
		}
	}
	
	static func getDrinkByID(drinkIdentifier: String , completionHandler completion:@escaping (_ success:Bool, _ error : NSError?) -> Void){
		
		NetworkManager.get(endpoint: .drinksByID(drinkID: drinkIdentifier)) { (response, errorResponse) in
			
			if let responseAux = response {
				
				let drinksServerModels = try! responseAux.map([DrinkResponse].self, atKeyPath: "drinks")
				self.arraySingleDrinks = ServerMapper.mapSeverToDrinkUIModels(drinks: drinksServerModels)
				Singleton.sharedInstance.singleDrink = self.arraySingleDrinks
				completion(true, nil)
			
			} else if let error = errorResponse {
				print(error.localizedDescription)
				completion(false, nil)
			}
		}
	}
	
}
