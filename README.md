# My Cocktail

My cocktail, is a simple application that is easy to use every day for the preparation of drinks.

## Getting Started

For the purposes of running on your local machine for development and testing purposes, you need to install a dependency manager.

### Prerequisites

cocoaPods dependency manager and Moya


### Installing

For the cocoaPods dependency manager installation.

```
1. Open terminal 
```
```
2. Command on terminal: sudo gem install cocoapods
```
```
3. Set your project path on terminal.
```
```
4. Command : pod install
```
```
5. Close project of Xcode
```
```
6. In your project folder open My Cocktail.xcworkspace
```

While for Moya, when performing the pod install command, the version of moya that is written in pod file will be installed. 

## Running the tests

No unit tests were performed


## Deployment

After performing the above steps, the application will be ready to run.

## Additional Notes

The MVC architecture was used, each controller was responsible for a single storyboard, so the view layer would be easy to read, the network layer was separated into 3 parts, Requester, NetworkManager and MyCocktailAPI, all url configuration is the last one. I used singleton so that the data once read did not need to be called by the request again, besides I did not need to consume data locally since it would be only sweating the data cast, the util folder has two files that provides some help as transforms the data that came from the server into a model UI class, thus abandoning the old response class model and also downloading images from a single url.

The big point is that to be in the application cache you must have entered only once, assuming you chose a particular category, came all the drinks of that category you chose, entered the details of 1 drink, everything is saved so far, so It is possible to turn off the internet and replicate the same steps, without even a data consumption post everything is in cache memory.
so the application does not cache the data you have not walked through yet.

Consumption of the entire database to save locally is possible but the device may not have enough memory to support the entire database. 

## Built With

* [theCocktailDB](https://www.thecocktaildb.com/api.php) - Where data consumption came from
* [CocoaPods](https://cocoapods.org/) - Dependency Management
* [Moya](https://github.com/Moya/Moya) - Urls configuration was done

## Versioning

* [Gitlab](http://gitlab.com) for versioning. 

## Authors

* **Paulo Gutemberg** - [Linkedin](https://www.linkedin.com/in/paulo-gutemberg-8722aaa6/)


