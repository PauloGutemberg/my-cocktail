//
//  Drink.swift
//  My Cocktail
//
//  Created by Paulo Gutemberg on 11/09/19.
//  Copyright © 2019 Paulo Gutemberg. All rights reserved.
//

import Foundation

public struct Drink {
	
	let id: String?
	let name: String?
	var category: String?
	let iba: String?
	let alcoholic: String?
	let glass: String?
	var instructions: String?
	let drinkThumbUrl: String?
	var ingredients: Array<Ingredient>?

}
