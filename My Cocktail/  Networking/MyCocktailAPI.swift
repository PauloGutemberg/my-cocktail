//
//  MyCocktailAPI.swift
//  My Cocktail
//
//  Created by Paulo Gutemberg on 14/09/19.
//  Copyright © 2019 Paulo Gutemberg. All rights reserved.
//

import Foundation
import Moya

enum MyCocktail {
	
	case categories
	case drinksByCategory(categoryName: String)
	case drinksByID(drinkID: String)
	
}

extension MyCocktail: TargetType {
	var baseURL: URL {
		return URL(string: "http://www.thecocktaildb.com/api/json/v1/1/")!
		
	}
	
	var path: String {
		switch self {
		case .categories:
			return "list.php"
		case .drinksByCategory:
			return "filter.php"
		case .drinksByID:
			return "lookup.php"
		}
	}
	
	var method: Moya.Method {
		return .get
	}
	
	var sampleData: Data {
		return Data()
	}

	var task: Task {
		switch self {
		case .categories:
			return .requestParameters(parameters: ["c": "list"], encoding: URLEncoding.queryString)
		case .drinksByCategory(categoryName: let categoryName):
			return .requestParameters(parameters: ["c": categoryName], encoding: URLEncoding.queryString)
		case .drinksByID(drinkID: let drinkIdentifier):
			return .requestParameters(parameters: ["i": drinkIdentifier], encoding: URLEncoding.queryString)
		}
	}
	
	var headers: [String : String]? {
		return ["Content-type": "application/json"]
	}
		
}

private extension String {
	var urlEscaped: String {
		return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
	}
	
	var utf8Encoded: Data {
		return data(using: .utf8)!
	}
}
